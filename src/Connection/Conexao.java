package Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {

    static String url= "jdbc:mysql://localhost:3306/Loja?useTimezone=true&serverTimezone=UTC";
    static Connection con;
    
    public static Connection conectar() throws ClassNotFoundException{
    
        try {
            //Class.forName("com.mysql.jc.jdbc.Drive");
            con= DriverManager.getConnection(url, "root", "root");
            System.out.println("CONEXÃO OK!");
        } catch (SQLException ex) {
            Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("CONEXÃO FALHOU!");
        }
    
        return con;
        
    }
}