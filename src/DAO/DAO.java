package DAO;

import Connection.Conexao;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import loja.Produto;

public class DAO {
    
    public void inserir(Produto p) throws ClassNotFoundException, SQLException{
    
        String sql= "insert into produto(nome, descricao, marca, categoria, preco_custo, preco_venda)"
                + "values(?,?,?,?,?,?)";
 
        
        PreparedStatement pps;
        pps=Conexao.conectar().prepareStatement(sql);
        pps.setString(1, p.getNome());
        pps.setString(2, p.getDescricao());
        pps.setString(3, p.getMarca());
        pps.setString(4, p.getCategoria());
        pps.setDouble(5, p.getPrecoCusto());
        pps.setDouble(6, p.getPrecoVenda());
        
        pps.executeUpdate();
    }
    
}
